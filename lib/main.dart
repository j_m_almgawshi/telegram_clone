import 'package:flutter/material.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
   @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar( 
          leading: Builder(
    builder: (BuildContext context) {
      return IconButton(
        icon: const Icon(Icons.menu),
        onPressed: () { Scaffold.of(context).openDrawer(); },
        tooltip: MaterialLocalizations.of(context).openAppDrawerTooltip,
      );
    },
  ),
          backgroundColor: Colors.blue.shade500,
            title: Text(
            "Telegram",
            style: TextStyle(
              color: Colors.white,
              fontSize: 20,
              fontWeight: FontWeight.bold,
            ),
          ),
          actions: <Widget>[
            IconButton(
              onPressed: (){},
              icon: const Icon(Icons.search),
            )
          ],
        ),
        body: ListView(
          children: <Widget>[
            Row(                            
              children: <Widget>[
              Container(                
                  width: 60,
                  height: 60, 
                  margin: EdgeInsets.all(10),                 
                  decoration: BoxDecoration(shape: BoxShape.circle,
                  color: Colors.blue.shade500,                                 
                  )
                ),
              Expanded(
                child: Row(                  
                  children: <Widget>[
                    Container(                      
                      height: 70, 
                      width:240,                           
                      decoration: const BoxDecoration(
                        color: Colors.white,
                        border: Border(        
                          bottom: BorderSide(width: 0.5, color: Color(0xFFFF7F7F7F)),
                        ),
                      ),
                      child:
                      Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,                                                
                          children: <Widget>[                            
                                Row(
                                  children: <Widget>[
                                    Row(
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    children: <Widget>[
                                    Text("Saved Messages",
                                    style: TextStyle(
                                    color: Colors.black,
                                    fontSize: 18,
                                    fontWeight: FontWeight.bold,
                                    ),
                                  ),
                                    ],
                                    ),
                                  Row(
                                    mainAxisAlignment: MainAxisAlignment.end,
                                    children: <Widget>[
                                  Text("10:23pm",
                                    style: TextStyle(
                                    color: Colors.grey.shade500,
                                    fontSize: 12,
                                    fontWeight: FontWeight.normal,
                                    ),
                                  ),
                                  ],
                                ),    
                                  ],
                                ),
                                
                              ],                                         
                        ),
                        Row(                                  
                                  children:[
                                    Text("You:",
                                      style: TextStyle(
                                      color: Colors.blue.shade500,
                                      fontSize: 16,
                                      fontWeight: FontWeight.normal,
                                      ),
                                    ),
                                    Text("bla bla bla",
                                      style: TextStyle(
                                      color: Colors.grey.shade500,
                                      fontSize: 16,
                                      fontWeight: FontWeight.normal,
                                      ),
                                    ),
                                    Icon(
                                    Icons.play_arrow,//the icon has to be changed
                                    color: Colors.grey.shade500,
                                    size: 25.0,
                                  ),
                                  ]
                                ),
                                ],
                    ),
                    ),

                  ]
                ),
                  
              ),
              ]
            ),
            
  ],
)
        /*Column(
          children: [
           Row(
             children: <Widget>[
               Container(
                 width: 60,
                 height: 60,
                 decoration: BoxDecoration(shape: BoxShape.circle,
                 color: Colors.blue.shade500,                 
                 )                                    
               ),
               Column(
                 mainAxisAlignment: MainAxisAlignment.spaceAround,
                 children: [
                 Row(
                  children:[
                    Text("Saved Messages",
                      style: TextStyle(
                      color: Colors.black,
                      fontSize: 18,
                      fontWeight: FontWeight.bold,
                      ),
                    ),                   

                  ]
                  ),
                 Row(
                   children:[
                    Text("bla bla bla",
                      style: TextStyle(
                      color: Colors.grey.shade400,
                      fontSize: 16,
                      fontWeight: FontWeight.normal,
                      ),
                    ),
                   ]
                 ),
                 ]
               ),
               Column(
                 children: [
                 Text("10:23",
                      style: TextStyle(
                      color: Colors.grey.shade400,
                      fontSize: 12,
                      fontWeight: FontWeight.normal,
                      ),
                    ),
                 //PlaceHolder(),
                 ]
               )
             ],
           ),
          ]
        
        ),*/
      )
    );
  }
}